# Custom CSS Class To Body

This module is used to add custom class to <body> tag specific to
node.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/custom_css_class_to_body).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/custom_css_class_to_body).

## Installation
### Prerequisites
- Drupal 8.x, 9.X, 10.X or 11.X
  
### Steps
You can install this module with two ways.

#### Via Composer
    1.  Navigate to your Drupal root directory.
    2.  Run the following command to install the module:
        ``` bash
        composer require drupal/custom_css_class_to_body  
        ```     
    3. After the module is installed, enable it using
   Drush or through the Drupal admin interface.

#### Manual Installation
    1. Download the module from [Drupal.org](https://www.drupal.org/project/custom_css_class_to_body).
    2. Extract the downloaded file and place it in the 
   `modules/contrib` directory of your Drupal installation.
    3. Enable the module using Drush or through the Drupal admin interface.
  
## Configuration

This module adds a "Custom Body Class Settings" fieldset in the content
type form.

There are two fields.

1. A textbox to add custom body class. Add multiple classes separated by space.
2. A checkbox,which when enabled add a node type as a class in <body> tag.
    For example: If a node type is article, then 'article' get added as a class in
    <body> tag of the page.

## Send Us an Email
Alternatively, you can send an email to our support team:
[hello@skynettechnologies.com](mailto:hello@skynettechnologies.com)

## Credits
This addon is developed and maintained by [Skynet Technologies USA LLC](https://www.skynettechnologies.com)

## Current Maintainers
- [Skynet Technologies USA LLC](https://www.drupal.org/skynet-technologies-usa-llc)
